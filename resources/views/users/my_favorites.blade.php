@extends('layouts.app')


@section('content')
    <div class="container">                
        @foreach ($myFavorites as $myFavorite)
        <div class="row ">
                <div class="col-6 offset-3">
                    <div class="pr-3">
                        <img src="{{$myFavorite->user->profile->profileImage()}}" alt="user-profileimage" class="rounded-circle w-100" style="max-width:40px;">
                        <span class="font-weight-bold">
                            <a href="/profile/{{$myFavorite->user->id}}">
                                <span class="text-dark">{{ $myFavorite->user->username }}</span>
                            </a>
                            </span>
                    </div>
                    <div class="pt-1">
                    
                        <p> 
                            <span class="font-weight-bold">
                            <a href="/profile/{{$myFavorite->user->id}}">
                                <span class="text-dark">{{ $myFavorite->user->username }}</span>
                            </a></span>
                            {{ $myFavorite->caption}}
                        </p>
                    </div>
                </div>
            </div>
        <div class="row pt-1 pb-4">
                <div class="col-6 offset-3">
                    <a href="/profile/{{ $myFavorite->user_id}}"><img src="/storage/{{$myFavorite->image}}" alt="post-image" class="w-100"></a> 
                    @if (Auth::check())
                    <div class="panel-footer pt-4">
                        <favorite
                            :post={{ $myFavorite->id }}
                            :favorited={{ $myFavorite->favorited() ? 'true' : 'false' }}
                        ></favorite>
                    </div>
                    @endif
                </div>
                
        </div>
        
        @endforeach
        
    </div>
@endsection 