@extends('layouts.app')


@section('content')
    <div class="container">                
        @foreach ($users as $user)
        @if (auth()->user()->id != $user->id)

        <div class="row pb-4">
                <div class="col-6 offset-3 d-flex justify-content-between align-items-baseline">
                    <div class="pr-3">
                        <img src="{{$user->profile->profileImage()}}" alt="user-profileimage" class="rounded-circle w-100" style="max-width:40px;">
                        <span class="font-weight-bold">
                            <a href="/profile/{{$user->id}}">
                                <span class="text-dark">{{ $user->username }}</span>
                            </a>
                            <span class="text-dark">{{ $user->profile->description }}</span>
                            </span>
                    </div>
                    @cannot('update', $user->profile)
                        <follow-button user-id="{{ $user->id }}" follows="{{(auth()->user()) ? auth()->user()->following->contains($user->id) : false}}"></follow-button>    
                    @endcannot  
                </div>
            </div>          
            @endif
        @endforeach
        
    </div>
@endsection 