@extends('layouts.app')


@section('content')
    <div class="container">                
        @foreach ($posts as $post)
        <div class="row ">
                <div class="col-6 offset-3">
                    <div class="pr-3">
                        <img src="{{$post->user->profile->profileImage()}}" alt="user-profileimage" class="rounded-circle w-100" style="max-width:40px;">
                        <span class="font-weight-bold">
                            <a href="/profile/{{$post->user->id}}">
                                <span class="text-dark">{{ $post->user->username }}</span>
                            </a>
                            </span>
                    </div>
                    <div class="pt-1">
                    
                        <p> 
                            <span class="font-weight-bold">
                            <a href="/profile/{{$post->user->id}}">
                                <span class="text-dark">{{ $post->user->username }}</span>
                            </a></span>
                            {{ $post->caption}}
                        </p>
                    </div>
                </div>
            </div>
        <div class="row pt-1 pb-4">
                <div class="col-6 offset-3">
                    <a href="/profile/{{ $post->user_id}}"><img src="/storage/{{$post->image}}" alt="post-image" class="w-100"></a> 
                    @if (Auth::check())
                    <div class="panel-footer pt-4">
                        <favorite
                            :post={{ $post->id }}
                            :favorited={{ $post->favorited() ? 'true' : 'false' }}
                        ></favorite>
                    </div>
                    @endif
                </div>
                
        </div>
        
        @endforeach
        <div class="row">
            <div class="col-12 d-flex justify-content-center">
                {{$posts->links()}}
            </div>
        </div>
    </div>
@endsection 