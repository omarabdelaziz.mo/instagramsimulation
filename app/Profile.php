<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\This;

class Profile extends Model
{

    protected $guarded = []; 

    public function profileImage()
    {
        $imagePath = ($this->image) ?  $this->image : 'profile/xfMS4IOm1B9gUoZLgjxOwJ6oXsJYnMv6A7ZJnTX9.jpeg'; 
        return '/storage/'. $imagePath;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    //follow relation ship
    public function followers()
    {
        return $this->belongsToMany(User::class);
    }
    
}
