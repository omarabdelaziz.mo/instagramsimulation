<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Intervention\Image\Facades\Image;
class PostsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $users = auth()->user()->following()->pluck('profiles.user_id');

        $posts = Post::whereIn('user_id', $users)->with('user')->latest()->paginate(5);
        
        return view('posts.index', compact('posts'));
    }

    public function create()
    {
        return view('posts.create');
    }

    public function store()
    {
        //validating request
        $data = request()->validate([
            
            'caption' =>['required'],
            'image' =>['required','image']
        ]);
        $imagePath = request('image')->store('uploads','public');

        // resizing the image
        $image = Image::make(public_path("/storage/{$imagePath}"))->fit(1200,1200);
        $image->save();
        auth()->user()->posts()->create([
            'caption' => $data['caption'],
            'image' => $imagePath
        ]);
        
        return redirect('/profile/'.auth()->user()->id);
    }

    public function show(Post $post)
    {
        $follows = (auth()->user()) ? auth()->user()->following->contains($post->user_id) : false;

        return view('posts.show', compact('post','follows'));
    }

    /* Favorite a particular post
    *
    * @param  Post $post
    * @return Response
    */
   public function favoritePost(Post $post)
   {
       auth()->user()->favorites()->attach($post->id);
   
       return back();
   }
   
   /**
    * Unfavorite a particular post
    *
    * @param  Post $post
    * @return Response
    */
   public function unFavoritePost(Post $post)
   {
       auth()->user()->favorites()->detach($post->id);
   
       return back();
   }
}
