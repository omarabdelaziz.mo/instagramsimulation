<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
class UsersController extends Controller
{
    /**
     * Get all favorite posts by user
     *
     * @return Response
     */
    public function myFavorites()
    {
        $myFavorites = auth()->user()->favorites;

        return view('users.my_favorites', compact('myFavorites'));
    }   

    /**
     * Search for Users Using Username
     * 
     * @return view 
     */
    public function searchForUser()
    {
        $data = request()->validate([
            'u' => 'required'
        ]);
        $users = User::where('username', $data['u'])
                        ->orWhere('username','like', '%'.$data['u'].'%')->get();
        return view('users.search', compact('users'));                
    }
}
